<?php
/**
 * @file
 * photo_albums.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function photo_albums_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'photo_blocks';
  $context->description = 'Ad blocks on photos pages';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'node_gallery_gallery' => 'node_gallery_gallery',
        'node_gallery_item' => 'node_gallery_item',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ad_block_nodequeues-block_6' => array(
          'module' => 'views',
          'delta' => 'ad_block_nodequeues-block_6',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Ad blocks on photos pages');
  $export['photo_blocks'] = $context;

  return $export;
}
