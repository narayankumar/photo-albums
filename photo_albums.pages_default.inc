<?php
/**
 * @file
 * photo_albums.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function photo_albums_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'gallery_node_add';
  $page->task = 'page';
  $page->admin_title = 'Gallery node add';
  $page->admin_description = '';
  $page->path = 'pet-photos/add';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'create node_gallery_gallery content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'action',
    'title' => 'Upload your Pet Photo Album',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_gallery_node_add_menu_context';
  $handler->task = 'page';
  $handler->subtask = 'gallery_node_add';
  $handler->handler = 'menu_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Context Admin',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'context_admin_options' => 'node_create_menu',
    'context_admin_options_items' => 'node_gallery_gallery',
    'submitted_context' => 'empty',
    'context_admin_use_node_edit' => 1,
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['gallery_node_add'] = $page;

  return $pages;

}
