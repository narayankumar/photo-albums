<?php
/**
 * @file
 * photo_albums.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function photo_albums_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function photo_albums_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function photo_albums_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: ad_block_photos
  $nodequeues['ad_block_photos'] = array(
    'name' => 'ad_block_photos',
    'title' => 'Ad block - Photos',
    'subqueue_title' => '',
    'size' => 4,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'advertisement',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: editors_photo_picks
  $nodequeues['editors_photo_picks'] = array(
    'name' => 'editors_photo_picks',
    'title' => 'Editor\'s photo picks',
    'subqueue_title' => '',
    'size' => 3,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'node_gallery_item',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_node_info().
 */
function photo_albums_node_info() {
  $items = array(
    'node_gallery_gallery' => array(
      'name' => t('Album'),
      'base' => 'node_content',
      'description' => t('An album of user photos'),
      'has_title' => '1',
      'title_label' => t('Album Name'),
      'help' => '',
    ),
    'node_gallery_item' => array(
      'name' => t('Photo'),
      'base' => 'node_content',
      'description' => t('A single photo which is part of a Photo Album'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
