
build2014091801
- made title 'pet photos' in teasers view (manual add)
- gave it class=black, h4,strong
- changed page manager setting to show pet-photos/add
- changed path for album and pet photos
- changed name of CT to album instead of photo album

build2014091602
- provide comments box

build2014091501
- made comment form visible, no ajax

7.x-1.9-dev1
- initial commit on 12 sep 2014
